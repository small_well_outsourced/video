App({
	onLaunch: function (options) {
    var that = this;
	},
  onShow: function (options) {
	},
	onHide: function () {
	},
	onError: function (msg) {
		console.log(msg)
	},
	//加载微擎工具类
	tabBar: {
		"color": "#123",
		"selectedColor": "#1ba9ba",
		"borderStyle": "#1ba9ba",
		"backgroundColor": "#fff",
		"list": [
			
		]
	},
	globalData: {
		userInfo: null,
	},
	//用户信息，sessionid是用户是否登录的凭证
	userInfo: {
		sessionid: null,
	},
  siteInfo: require('siteinfo.js'),
  module_name: 'hellozjx_mapwxapp',
  util: require('hellozjx_mapwxapp/resource/js/util.js'),
  app_host: 'http://weiqin.com/attachment/'
});