var app = getApp();

function getIndex(area_id, cb) {
  app.util.request({
    'url': 'entry/wxapp/index',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      area_id: area_id ? area_id : 0
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function getMapIndex(area_id, cb) {
  app.util.request({
    'url': 'entry/wxapp/MapIndex',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      area_id: area_id ? area_id : 0
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function getCoupon(cb) {
  app.util.request({
    'url': 'entry/wxapp/getcoupon',
    'cachetime': '0',
    'data': {
      m: app.module_name,
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function doAuthCheck(cb,openid='null') {
  wx.getUserInfo({
    withCredentials: true,
    success: function (res) {
      //此处为获取微信信息后的业务方法
      //console.log(res.userInfo)
      typeof cb == "function" && cb(res.userInfo);
    },
    fail: function () {
      //获取用户信息失败后。请跳转授权页面
      wx.redirectTo({
        url: '../login/login',
      })
    }
  })
}
function doLike(id, type, cb) {
  app.util.request({
    'url': 'entry/wxapp/like',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      type: type ? type : 'shop',
      id: id ? id : 0
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function doComment(id, content, cb) {
  app.util.request({
    'url': 'entry/wxapp/comment',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      content: content,
      id: id ? id : 0
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function doSearch(keyword,page, cb) {
  app.util.request({
    'url': 'entry/wxapp/keyword_new',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      keyword: keyword,
      page: page
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function doUserCenter(cb) {
  app.util.request({
    'url': 'entry/wxapp/usercenter',
    'cachetime': '0',
    'data': {
      m: app.module_name,
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function myCollect(page, cb) {
  app.util.request({
    'url': 'entry/wxapp/mycollect_new',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      page:page
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function myCoupon(cb) {
  app.util.request({
    'url': 'entry/wxapp/mycoupon',
    'cachetime': '0',
    'data': {
      m: app.module_name,
    },
    success: function (res) {
        typeof cb == "function" && cb(res);
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function myTicket(cb) {
  app.util.request({
    'url': 'entry/wxapp/myticket',
    'cachetime': '0',
    'data': {
      m: app.module_name,
    },
    success: function (res) {
      typeof cb == "function" && cb(res);
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function useCoupon(order_id, password, cb) {
  app.util.request({
    'url': 'entry/wxapp/usecoupon',
    'cachetime': '0',
    'data': {
      order_id: order_id,
      password: password,
      m: app.module_name,
    },
    success: function (res) {
      typeof cb == "function" && cb(res);
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function useTicket(order_id, password, cb) {
  app.util.request({
    'url': 'entry/wxapp/useticket',
    'cachetime': '0',
    'data': {
      order_id: order_id,
      password: password,
      m: app.module_name,
    },
    success: function (res) {
      typeof cb == "function" && cb(res);
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function getCopyright(cb) {
  app.util.request({
    'url': 'entry/wxapp/copyright',
    'cachetime': '0',
    'data': {
      m: app.module_name,
    },
    success: function (res) {
      typeof cb == "function" && cb(res);
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function getCouponList(page,cb) {
  app.util.request({
    'url': 'entry/wxapp/getCouponList',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      page: page,
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function GetMapList(lat,lng,area_id,cb) {
  app.util.request({
    'url': 'entry/wxapp/GetMapList',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      area_id: area_id ? area_id : 0,
      lat:lat,
      lng:lng,
    },
    success: function (res) {
      console.log(res);
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function getGoods(sort_id,area_id,page,cb) {
  app.util.request({
    'url': 'entry/wxapp/get_goods',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      area_id: area_id ? area_id : 0,
      sort_id: sort_id ? sort_id : 0,
      page: page,
    },
    success: function (res) {
      console.log(res);
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function getArticle(page, cb) {
  app.util.request({
    'url': 'entry/wxapp/getArticle',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      page: page,
    },
    success: function (res) {
      console.log(res);
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
function getList(page, cb) {
  app.util.request({
    'url': 'entry/wxapp/get_list',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      page: page
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function getComment(aid, page, cb) {
  app.util.request({
    'url': 'entry/wxapp/comment_list',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      aid: aid,
      page: page
    },
    success: function (res) {
      if (res.data.message.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

function failGo(content) {
  wx.showModal({
    content: content ? content : '',
    showCancel: false,
    success: function (res) {
      if (res.confirm) {
        wx.redirectTo({
          url: 'pages/index/index'
        })
      }
    }
  })
}

function doAlert(content, cb = '') {
  wx.showModal({
    content: content ? content : '',
    showCancel: false,
    success: function (res) {
      typeof cb == "function" && cb(res);
    }
  })
}
function contains(a, obj) {
  var i = a.length;
  while (i--) {
    if (a[i] === obj) {
      return true;
    }
  }
  return false;
}

/**
 * 首页轮播图
 */
function indexBanner(cb) {
  app.util.request({
    'url': 'entry/wxapp/Banner',
    'cachetime': '0',
    'data': {
      m: app.module_name
    },
    success: function (res) {
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
// 获取所有课堂类型
function indexCourseType(cb) {
  app.util.request({
    'url': 'entry/wxapp/Room',
    'cachetime': '0',
    'data': {
      m: app.module_name
    },
    success: function (res) {
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

// 会员注册
function userRegister(param, cb) {
  app.util.request({
    'url': 'entry/wxapp/UserRegister',
    'cachetime': '0',
    'data': {
      real_name: param.nickName,
      sex: param.sex,
      nick_name: param.nickName,
      avatar: param.avatarUrl,
      m: app.module_name
    },
    success: function (res) {
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
// 检测视频是否购买
function isVideoPay(id, cb) {
  app.util.request({
    'url': 'entry/wxapp/isVideoPay',
    'cachetime': '0',
    'data': {
      id: id,
      m: app.module_name
    },
    success: function (res) {
      console.log(res.data);
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

// 获取课堂对应的视频
function indexVideoList(room_id, cb) {
  app.util.request({
    'url': 'entry/wxapp/Video',
    'cachetime': '0',
    'data': {
      room_id: room_id,
      m: app.module_name
    },
    success: function (res) {
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}
// 获取老师详情页
function teacherDetail(id, cb) {
  app.util.request({
    'url': 'entry/wxapp/TeacherDetail',
    'cachetime': '0',
    'data': {
      id: id,
      m: app.module_name
    },
    success: function (res) {
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}

// 入住加盟
// 获取老师详情页
function enterData(obj,cb) {
  app.util.request({
    'url': 'entry/wxapp/enter',
    'cachetime': '0',
    'data': {
      m: app.module_name,
      name          :   obj.name,
      sex           :   obj.sex,
      email         :   obj.email,
      phone         :   obj.phone,
      id_card       :   obj.id_card,
      qualification :   obj.is_certificate,
      latitude      :   obj.latitude,
      longitude     :   obj.longitude,
      city          :   obj.city,
      address       :   obj.address
    },
    success: function (res) {
      if (res.data.errno == 0) {
        typeof cb == "function" && cb(res);
      } else {
        failGo(res.data.message);
      }
    },
    fail: function () {
      failGo('请检查连接地址');
    }
  })
}


module.exports = {
  'getIndex': getIndex,
  'doLike': doLike,
  'doComment': doComment,
  'doSearch': doSearch,
  'doUserCenter': doUserCenter,
  'myCollect': myCollect,
  'myCoupon': myCoupon,
  'myTicket': myTicket,
  'useCoupon':useCoupon,
  'useTicket': useTicket,
  'failGo': failGo,
  'doAlert': doAlert,
  'contains': contains,
  'getCopyright': getCopyright,
  'getGoods': getGoods,
  'getList': getList,
  'getComment': getComment,
  'doAuthCheck': doAuthCheck,
  'getCoupon': getCoupon,
  'getCouponList': getCouponList,
  'getMapIndex': getMapIndex,
  'GetMapList': GetMapList,
  'getArticle': getArticle,
  // 自定义
  'isVideoPay': isVideoPay,
  'userRegister': userRegister,
  'indexBanner': indexBanner,
  'indexCourseType': indexCourseType,
  'indexVideoList': indexVideoList,
  'teacherDetail': teacherDetail,
  'enterData': enterData,
};