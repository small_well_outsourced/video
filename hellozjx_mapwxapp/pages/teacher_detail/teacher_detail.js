var js = require('../../resource/js/app.js');
var app = getApp()

var template = require('../custom_tab/custom_tab.js');
Page({
  data: {
    is_charge: 0, //视频是否收费 
    money: 0, //视频价格
    id: 1,  //视频 id
    // 试听视频图片
    teacher_vedio_img: '/static/image/cover.png',
    // 详情
    detail:{
      name      :   '王雪峰',
      course    :   [],
      teach_age :   8,
      flags     :   [],
      tabs      :   [
        { id: 0, text: "在线课程", alive: true},
        { id: 1, text: "上门课程", alive: false }
      ]
    },
    // 商品信息
    shop:{
      name    :  '在线1对1辅导',
      remark  :  '让学习更高效'
    },
    // 地址
    address:  '江苏省南京市鼓楼区新城市广场',
    // 手机号
    phone:  '021-88885555',
    // 作品、评论、资料
    works:[
      {id:  0,  text: "作品", alive: false },
      {id:  1,  text: "评论", alive: true },
      {id:  2,  text: "资料", alive: false }
    ],
    // 印象标签
    impression:[
      { text: '善于沟通', num: 245 },
      { text: '氛围轻松', num: 399 },
      { text: '思路清晰', num: 457 },
      { text: '效果显著', num: 231 },
      { text: '口齿清晰', num: 412 },
      { text:'重难点分析透彻',num:131}
    ],
    // 学生评价
    evaluate:[
      { src: '/static/image/portrait.png', name: '学生小明', date: '2019-01-06', remark: '', content:'这个教程很好这个教程很好这个教程很好这个教程很好这个教程很好这个教程很好这个教程很好'},
      { src: '/static/image/portrait.png', name: '学生小明', date: '2019-01-06', remark: '', content: '这个教程很好这个教程很好这个教程很好这个教程很好这个教程很好这个教程很好这个教程很好' }
    ]

  },
  onLoad:function(options) {
    var that = this;
    // that.setData({
    //   id: options.id
    // })
    // 底部菜单
    template.tabbar("tabBar", 0, this);
    that.detail();
  },
  // 更换课程菜单
  changeTab:function(event){
    var id     = event.target.dataset.id;
    var tab    = event.target.dataset.tab;

    switch (tab){
      case 'class':
        var data = this.data.detail.tabs;
        var str = "detail.tabs";
        break;
      case 'work':
        var data = this.data.works;
        var str = "works";
        break;
    }
    // 删除已选中
    for (var index in data) {
      if (data[index].alive == true) {
        var old_a = str + "[" + index + "].alive";
        this.setData({
          [old_a]: false
        })
      }
    }
    // 新选中
    var new_a = str + "[" + id + "].alive";
    this.setData({
      [new_a]: true,
    });
  },
  // 获取老师详情
  detail: function () {
    var that = this;
    js.teacherDetail(that.data.id, function (res) {
      console.log(res.data.data);
      that.setData({
        'detail.name': res.data.data.teacher_name,
        'detail.course': res.data.data.subject,
        'detail.teach_age': res.data.data.teach_age,
        'detail.flags': res.data.data.attr,
        'address': res.data.data.address,
        'phone': res.data.data.phone,
        'money': res.data.data.price,
        'is_charge': res.data.data.is_charge
      })
    });
  },
  //免费试听
  video: function () {
    var that = this;
    // 判断是免费还是收费视频
    if (that.data.is_charge > 0) {
      // 检测视频是否购买
      js.isVideoPay(that.data.id, function(res) {
        wx.navigateTo({
          url: '../vedio/vedio?vid=' + that.data.id,
        })
      })
      return false;
    }
    wx.navigateTo({
      url: '../vedio/vedio?vid=' + that.data.id,
    })
  },
  updateUserInfo(result) {
    var app = getApp()
    var that = this;
    //拿到用户数据时，通过app.util.getUserinfo将加密串传递给服务端
    //服务端会解密，并保存用户数据，生成sessionid返回
    app.util.getUserInfo(function (userInfo) {
      //这回userInfo为用户信息
      console.log(userInfo)
      js.userRegister({
        nickName: userInfo.wxInfo.nickName,
        sex: userInfo.wxInfo.gender,
        avatarUrl: userInfo.wxInfo.avatarUrl
      }, function(res) {
        // 会员注册成功才发起支付
        that.pay();
      })
    }, result.detail)
  },
  pay: function(options) {
    var that = this;
    app.util.request({
      'url': 'entry/wxapp/pay', //调用wxapp.php中的doPagePay方法获取支付参数
      data: {
        vid: that.data.id,
        fee: that.data.money
      },
      'cachetime': '0',
      success(res) {
        if (res.data && res.data.data && !res.data.errno) {
          //发起支付
          wx.requestPayment({
            'timeStamp': res.data.data.timeStamp,
            'nonceStr': res.data.data.nonceStr,
            'package': res.data.data.package,
            'signType': 'MD5',
            'paySign': res.data.data.paySign,
            'success': function (res) {
              //执行支付成功提示
            },
            'fail': function (res) {
              backApp()
            }
          })
        }
      },
      fail(res) {
        wx.showModal({
          title: '系统提示',
          content: res.data.message ? res.data.message : '错误',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              backApp()
            }
          }
        })
      }
    })
  }
})

