//初始化数据
function tabbarinit() {
  return [
    {
      "current": 0,
      "pagePath": "/hellozjx_mapwxapp/pages/map/map?id=1",
      "iconPath": "/static/icon/brain.png",
      "selectedIconPath": "/static/icon/brain_a.png",
      "text": "语言课堂"
    },
    {
      "current": 0,
      "pagePath": "/hellozjx_mapwxapp/pages/map/map?id=2",
      "iconPath": "/static/icon/art.png",
      "selectedIconPath": "/static/icon/art_a.png",
      "text": "艺术课堂"

    },
    {
      "current": 0,
      "pagePath": "/hellozjx_mapwxapp/pages/map/map?id=3",
      "iconPath": "/static/icon/skill.png",
      "selectedIconPath": "/static/icon/skill_a.png",
      "text": "技能课堂"
    },
    {
      "current": 0,
      "pagePath": "/hellozjx_mapwxapp/pages/map/map?id=4",
      "iconPath": "/static/icon/science.png",
      "selectedIconPath": "/static/icon/science_a.png",
      "text": "科学课堂"
    }
  ]

}
//tabbar 主入口
function tabbarmain(bindName = "tabdata", id, target) {
  var that = target;
  var bindData = {};
  var otabbar = tabbarinit();
  otabbar[id]['iconPath'] = otabbar[id]['selectedIconPath']//换当前的icon
  otabbar[id]['current'] = 1;
  bindData[bindName] = otabbar
  that.setData({ bindData });
}

module.exports = {
  tabbar: tabbarmain
}
