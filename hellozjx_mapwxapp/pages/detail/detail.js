var js = require('../../resource/js/app.js');
var app = getApp()
Page({
  data: {
    lovenum:0,
    isLoved:0,
    lovenum:0,
    isPicked:0,
    isPlanted:0,
  },

  onLoad: function (options) {
    var id = options.id;
    if (id == 0) {
      wx.navigateBack({
      })
    }
    var that = this
    that.setData({
      id:id,
    })
    app.util.request({
        'url': 'entry/wxapp/detail',
        'cachetime': '0',
        'data': {
          m: app.module_name,
          id: id
        },
        success: function (res) {
          console.log(res.data.message.message)
          if (res.data.message.errno == 0) {
            that.setData({
              detail: res.data.message.message,
              title: res.data.message.message.name,
            });
            wx.setNavigationBarTitle({
              title: res.data.message.message.name
            });
          } else {

          }
        }
      });

  },
  phonecall:function(){
    wx.makePhoneCall({
      phoneNumber: this.data.detail.phone
    })
  },
  openAddress: function (e) {
    wx.openLocation({
      latitude: this.data.detail.shop_lat,
      longitude: this.data.detail.shop_lng,
      name: this.data.detail.name,
      address: this.data.detail.address,
      fail: function (res) {
        console.log(res);
      }
    });
  },

  onReady: function () {

  },

  onShow: function () {

  },

  onHide: function () {

  },

  onUnload: function () {

  },
  onShareAppMessage: function () {
    var that = this;
    return {
      title: that.data.title,
      path: '/hellozjx_mapwxapp/pages/detail/detail?id='+that.data.id,
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})