//home.js
var js = require('../../resource/js/app.js');
Page({
  data: {
    // 是否可以提交
    is_submit : false
  },
  onLoad:function() {

  },
  // 同意协议
  change_agreement(e){
    let val  = e.detail.value[0];
    let temp = false;
    if(val == 1){
      temp = true;
    }
    this.setData({
      is_submit: temp,
    });
  },
  // 提交表单
  formSubmit(e) {
    var form = e.detail.value;
    console.log(form);
    for(var v in form){
      if(form[v] == ''){
        wx.showModal({
          title: '提示',
          content: '请填写完整'
        })
        return false;
      }
    }
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        form.latitude = res.latitude
        form.longitude = res.longitude
      }
    })
    // 调用接口
    js.enterData(form, function(res){
      console.log(res);
    })
  },
  
  

})

