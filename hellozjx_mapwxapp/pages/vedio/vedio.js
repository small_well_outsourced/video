Page({
  data: {
    // 用于记录当前播放的视频的索引值
    playIndex: null,
    // 视频列表
    courseList: [{
      videoUrl: 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400',
      coverUrl: '/static/image/video_cover.png', //视频封面图
    }],
    // 作者头像
    user_portrait:'/static/video/user_portrait.png',
    // 是否已经收藏
    is_collection:false,
    // 收藏数量
    collection_num: 1314,
    // 评论数量
    comment_num:228,
    // 是否精选
    is_boutique: true,
    // 分类
    category: '旅行摄影教程',
    // 作者
    author_name: '王雪峰老师',
    // 作者介绍
    explain: '小学语文   教龄8年',
    // 标签
    flags:[
      { text: '在线课程' },
      { text: '作文' },
      { text: '中考' },
      { text: '幽默风趣' },
    ]
  },
  // 点击收藏
  clickCollection:function(){
    this.setData({
      is_collection : !this.is_collection
    })
  },
  // 点击播放
  videoPlay: function (e) {
    var curIdx = e.currentTarget.dataset.index;
    // 没有播放时播放视频
    if (!this.data.playIndex) {
      this.setData({
        playIndex: curIdx
      })
      var videoContext = wx.createVideoContext('video' + curIdx) //这里对应的视频id
      videoContext.play()
    } else { // 有播放时先将prev暂停，再播放当前点击的current
      var videoContextPrev = wx.createVideoContext('video' + this.data.playIndex)
      if (this.data.playIndex != curIdx) {
        videoContextPrev.pause()
      }
      this.setData({
        playIndex: curIdx
      })
      var videoContextCurrent = wx.createVideoContext('video' + curIdx)
      videoContextCurrent.play()
    }
  }

})

