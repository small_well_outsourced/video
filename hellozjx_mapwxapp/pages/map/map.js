var js = require('../../resource/js/app.js');
var app = getApp()
Page({
  data: {
    isready: false,
    currentTabIndex: 0,
    markers: [],
    resid: "",
    restaurants:[],
    index: 0,
    resList: [],
    longitude: '',
    latitude: '',
    scale: 15,
    plant: [],
    pick: [],
    dist: "无限制", 
    icon_type:0,
    list: [{id: 1, name: 222}]
  },
  locateTo:function(e){
    var that=this;
    var id = e.currentTarget.id;
    console.log(id)
    var index = that.data.markers.findIndex(function (ele) {
      if (ele.id == id ) {
        return true
      }
    });
    console.log(that.data.markers[index])
    var longitude = that.data.markers[index].longitude;
    var latitude = that.data.markers[index].latitude;
    var scale = 16;
    that.setData({
      resid:id,
      longitude: longitude,
      latitude: latitude,
      scale: scale,
    })

  },
  markertap: function (e){
    var index = e.markerId;
    console.log(index)
    wx.navigateTo({
      url: "../detail/detail?id=" + index
    });
  },
  bindPickerChange: function (e) {
    var that = this
    var selectid = e.detail.value;
    wx.setStorageSync('area_id', that.data.restaurants[selectid]['id']);
    wx.setStorageSync('selectid', selectid);
    this.setData({
      index: e.detail.value
    });
    that.getIndex();
  },
  getIndex: function () {
    var area_id = wx.getStorageSync('area_id');
    var that = this;
    js.getMapIndex(area_id, function (res) {
      console.log(res)
      wx.setNavigationBarTitle({
        title: res.data.message.message.config.title,
      })
      if (res.data.message.message.config.icon_type==1)
      {
        wx.downloadFile({
          url: res.data.message.message.config.icon, //仅为示例，并非真实的资源
          success: function (ret) {
            // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
            if (ret.statusCode === 200) {
              that.setData({
                icon_type:1,
                icon_path: ret.tempFilePath,
              })
            }
          }
        })
      }
      if (res.data.message.message.config.shop_lat=='')
      {
        var restaurants = res.data.message.message.area;
        var dist = res.data.message.message.config.distance;
        dist ? dist=dist+"千米":dist="无限制";
        wx.getLocation({
          type: 'gcj02',
          success: function (res) {
            //console.log(res);
            var latitude = res.latitude
            var longitude = res.longitude
            that.setData({
              dist:dist,
              restaurants: restaurants,
              latitude: latitude,
              longitude: longitude,
            })
            that.getMapList();
          }
        })
      }
      else
      {
        var dist = res.data.message.message.config.distance;
        dist ? dist=dist+"千米" : dist = "无限制";
        that.setData({
          dist:dist,
          restaurants: res.data.message.message.area,
          latitude: res.data.message.message.config.shop_lat,
          longitude: res.data.message.message.config.shop_lng,
        })
        that.getMapList();
      }
    })
  },
  download:function(url, cb) {
    wx.downloadFile({
      url: url, //仅为示例，并非真实的资源
      success: function (res) {
        typeof cb == "function" && cb(res);
      },
    })
  },
  getMapList: function () {
    var that = this;
    var area_id = wx.getStorageSync('area_id');
    var lat=that.data.latitude;
    var lng=that.data.longitude;
    js.GetMapList(lat,lng,area_id, function (res) 
    {
      if (res.data) {
        var data = res.data.message.message.list;
        var marker_new = [];
        var list_new = [];
        var include_new = [];
        var path="";
        
        data.forEach(function (val,index,arr) {
          var lnglat=[val.shop_lat_t, val.shop_lng_t]
          var iconInfo = {
            id: val.id,
            latitude: val.shop_lat_t,
            longitude: val.shop_lng_t,
            width: 35,
            height: 35,
            callout: {
              content: val.name,
              display: "ALWAYS",
              padding: 5,
              borderRadius: 50,
              bgColor: "#ffffff",
              color: "#333333",
              fontSize: 12
            }
          };
          
          if(that.data.icon_type==1) {
            iconInfo.iconPath=that.data.icon_path;
            marker_new.push(iconInfo);
            list_new.push({
              id: val.id,
              name: val.name,
              lnglat: [val.shop_lat_t, val.shop_lng_t]
            });
            include_new.push([lnglat[0], lnglat[1]]);
            that.setData({
              markers: marker_new,
              list: list_new,
            });
          } else {
            that.download(val.logo,function(ret){    
              if (ret.statusCode===200) {
                path = ret.tempFilePath;
                if(path=="") {
                  iconInfo.iconPath = '../../resource/images/mark.png';
                } else {                  
                  iconInfo.iconPath = path;
                }
              }
              marker_new.push(iconInfo);
              list_new.push({
                id: val.id,
                name: val.name,
                lnglat: [val.shop_lat_t, val.shop_lng_t]
              });
             
              include_new.push([lnglat[0], lnglat[1]]);
              that.setData({
                markers: marker_new,
                list: list_new,
              });
            })
          }
        }); 
      }
      console.log(that.data.list)
    })
  },
  onLoad: function (options) {
    var that=this;
  },

  onReady: function () {

  },

  onShow: function () {
    var that=this
    var area_id = wx.getStorageSync('area_id');
    if (area_id) {
      var selectid = wx.getStorageSync('selectid');
      that.setData({
        index: selectid
      })
    }
    that.getIndex();
  },

  onHide: function () {

  },

  onUnload: function () {

  },


})