var js = require('../../resource/js/app.js');
var app = getApp()
//home.js
Page({
  data: {
    app_host: app.app_host,
    // 当前城市
    current_city: '南京',
    // 封面图片路径
    cover_image: '/static/image/cover.png',
    // 课程选择标题
    course_title:[],
    // 课程列表
    course_list : [],
    room_id : 1 // 当前课堂 id
  },
  onLoad:function() {
    var that = this;
    that.indexBanner();
    that.courseType();
    that.videoList();
  },
  
  // 选择课程
  choiceCourse: function (e) {
    let id  = e.currentTarget.dataset.index;
    // 删除已选中
    for (var index in this.data.course_title){
      if (this.data.course_title[index].alive == true){
        let old = "course_title[" + index + "].alive";
        this.setData({
          [old] : false
        })
      }
    }
    // 新选中
    let str = "course_title[" + id + "].alive";
    this.setData({
      [str] : true,
      room_id: e.currentTarget.dataset.id
    });
    this.videoList();   
  },
  // 首页轮播图
  indexBanner: function () {
    js.indexBanner(function (res) {
    })
  },
  // 课堂类型
  courseType: function () {
    var that = this
    js.indexCourseType(function (res) {
      var data = res.data.data
      that.setData({
        course_title: data
      })
    })
  },
  // 课堂对应的视频列表
  videoList: function () {
    var that = this
    js.indexVideoList(that.data.room_id, function (res) {
      that.setData({
        course_list: res.data.data
      })
    })
  },
  // 点击跳转详情页
  detail: function (e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../teacher_detail/teacher_detail?id=' + id,
    })
  }
})

